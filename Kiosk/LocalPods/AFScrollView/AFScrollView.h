//
//  AFScrollView.h
//  AFScrollView
//
//  Created by Alvaro Franco on 3/13/14.
//  Copyright (c) 2014 AlvaroFranco. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AFScrollView;
@protocol AFScrollViewDelegate <UIScrollViewDelegate>

@optional
-(void)afScrollView:(UIScrollView *)theScrollView didSelectIndexPage:(NSInteger )theIndexPage;

@end

@interface AFScrollView : UIScrollView

@property (nonatomic, assign) id<AFScrollViewDelegate> AFDelegate;

typedef void (^configurationBlock)(UIView *view, NSInteger index, BOOL success);
-(id)initWithFrame:(CGRect)frame andNumberOfPages:(NSInteger)pages;
-(void)configureViewAtIndexWithCompletion:(configurationBlock)completion;

-(void)selectIndexPage:(NSInteger )theSelectIndexPage;

@end
