//
//  AFScrollView.m
//  AFScrollView
//
//  Created by Alvaro Franco on 3/13/14.
//  Copyright (c) 2014 AlvaroFranco. All rights reserved.
//

#import "AFScrollView.h"

@interface AFScrollView () <UIScrollViewDelegate>

@property (nonatomic) NSInteger numberOfPages;

@end

@implementation AFScrollView

-(id)initWithFrame:(CGRect)frame andNumberOfPages:(NSInteger)pages {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.contentSize = CGSizeMake(pages * self.bounds.size.width, self.bounds.size.height);
        _numberOfPages = pages;
        self.delegate = self;
    }
    
    return self;
}

-(void)configureViewAtIndexWithCompletion:(configurationBlock)completion {
    if (!_numberOfPages) {
        //NSLog(@"AFScrollView message: A valid number of views must be provided");
        completion(nil, 0, NO);
    } else {
        
        for (int i = 0; i < _numberOfPages; i++) {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(self.bounds.size.width * i, 0, self.bounds.size.width, self.bounds.size.height)];
            [self addSubview:view];
            
            completion(view, i, YES);
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{

}

-(void)selectIndexPage:(NSInteger )theSelectIndexPage;
{
    switch (theSelectIndexPage) {
        case 0: // день
            [self setContentOffset:CGPointMake(0, 0) animated:YES];
            break;
        case 1: //неделя
            [super setContentOffset:CGPointMake(CGRectGetWidth(self.bounds), 0) animated:YES];
            break;
        case 2: //месяц
            [super setContentOffset:CGPointMake(CGRectGetWidth(self.bounds)*2, 0) animated:YES];
            break;
        default:
            [self setContentOffset:CGPointMake(0, 0) animated:YES];
            break;
    }
}

@end
