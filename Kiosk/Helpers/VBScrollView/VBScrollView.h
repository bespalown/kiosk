//
//  VBScrollView.h
//  VBScrollView
//
//  Created by bespalown on 3/13/14.
//  Copyright (c) 2014 bespalown. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VBScrollView;
@protocol VBScrollViewDelegate <UIScrollViewDelegate>

@optional
-(void)VBScrollView:(VBScrollView *)theScrollView didSelectIndexPage:(NSInteger )theIndexPage;

@end

@interface VBScrollView : UIScrollView
{
    BOOL isSelectIndexPage;
}
@property (nonatomic, assign) id<VBScrollViewDelegate> VBDelegate;

typedef void (^configurationBlock)(UIView *view, NSInteger index, BOOL success);

-(id)initWithFrame:(CGRect)frame andNumberOfPages:(NSInteger)pages;
-(void)configureViewAtIndexWithCompletion:(configurationBlock)completion;

-(void)selectIndexPage:(NSInteger )theSelectIndexPage;

@end
