//
//  VBScrollView.m
//  VBScrollView
//
//  Created by bespalown on 3/13/14.
//  Copyright (c) 2014 bespalown. All rights reserved.
//

#import "VBScrollView.h"

@interface VBScrollView () <UIScrollViewDelegate>

@property (nonatomic, assign) NSInteger numberOfPages;

@end

@implementation VBScrollView

-(id)initWithFrame:(CGRect)frame andNumberOfPages:(NSInteger)pages {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.contentSize = CGSizeMake(pages * self.bounds.size.width, self.bounds.size.height);
        _numberOfPages = pages;
        self.delegate = self;
        isSelectIndexPage = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.backgroundColor = vbBlueColor;
    }
    
    return self;
}

-(void)configureViewAtIndexWithCompletion:(configurationBlock)completion {
    if (!_numberOfPages) {
        //NSLog(@"VBScrollView message: A valid number of views must be provided");
        completion(nil, 0, NO);
    } else {
        for (int i = 0; i < _numberOfPages; i++) {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(self.bounds.size.width * i, 0, self.bounds.size.width, self.bounds.size.height)];
            [self addSubview:view];
            
            completion(view, i, YES);
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    for (int i = 0; i < _numberOfPages; i ++) {
        if (scrollView.contentOffset.x == CGRectGetWidth(self.frame)*i) {
            [_VBDelegate VBScrollView:self didSelectIndexPage:i];
        }
    }
}

-(void)selectIndexPage:(NSInteger )theSelectIndexPage;
{
    [self setContentOffset:CGPointMake(CGRectGetWidth(self.bounds)*theSelectIndexPage, 0) animated:YES];
}

@end
