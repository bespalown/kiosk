//
//  Header.h
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#ifndef Kiosk_Header_h
#define Kiosk_Header_h

#define vbColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE YES//( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )

#define IS_IOS6 [[[UIDevice currentDevice] systemVersion] floatValue] >= 6 && [[[UIDevice currentDevice] systemVersion] floatValue] < 7
#define IS_IOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7 && [[[UIDevice currentDevice] systemVersion] floatValue] < 8
#define IS_IOS8 [[[UIDevice currentDevice] systemVersion] floatValue] >= 8

#define vbBlueColor [UIColor colorWithRed:0.075 green:0.533 blue:1.000 alpha:1.000]
#define vbOrangeColor [UIColor colorWithRed:1.000 green:0.714 blue:0.000 alpha:1.000]

#endif
