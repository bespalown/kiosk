//
//  VBTopHeaderLabel.h
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VBTopHeaderLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

-(void)setHeader:(NSString *)theHeader step:(NSUInteger )theStep fromSteps:(NSUInteger )theFromSteps;

-(CGFloat)getHeightLabel;

@end
