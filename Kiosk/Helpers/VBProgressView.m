//
//  VBProgressView.m
//  Kiosk
//
//  Created by bespalown on 08/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "VBProgressView.h"

@implementation VBProgressView

-(id)initWithStepsCount:(NSUInteger)theStepsCount frame:(CGRect )theFrame;
{
    if (self == [super initWithFrame:theFrame]) {
        _completeColor = vbOrangeColor;
        _bacgroundColor = vbBlueColor;
        stepsCount = theStepsCount;
        
        bacgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                 0,
                                                                 self.bounds.size.width,
                                                                 self.bounds.size.height)];
        [self addSubview:bacgroundView];
        
        completeView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                0,
                                                                1 * self.bounds.size.width / stepsCount,
                                                                self.frame.size.height)];
        [bacgroundView addSubview:completeView];
        
        [bacgroundView setBackgroundColor:_bacgroundColor];
        [completeView setBackgroundColor:_completeColor];
    }
    return self;
}

-(void)setCompleteColor:(UIColor *)completeColor
{
    [completeView setBackgroundColor:completeColor];
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    [bacgroundView setBackgroundColor:backgroundColor];
}

-(void)setStep:(NSUInteger )theStep animated:(BOOL)theAnimated;
{
    CGRect rect = CGRectMake(0,
                             0,
                             (theStep + 1) * self.bounds.size.width / stepsCount,
                             self.frame.size.height);
    if (theAnimated) {
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             [completeView setFrame:rect];
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
    else {
        [completeView setFrame:rect];
    }
}

@end
