//
//  VBProgressView.h
//  Kiosk
//
//  Created by bespalown on 08/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VBProgressView : UIView
{
    NSUInteger stepsCount;
    
    UIView *bacgroundView;
    UIView *completeView;
}

@property (nonatomic, strong) UIColor *completeColor;
@property (nonatomic, strong) UIColor *bacgroundColor;

-(id)initWithStepsCount:(NSUInteger)theStepsCount frame:(CGRect )theFrame;

-(void)setStep:(NSUInteger )theStep animated:(BOOL)theAnimated;

@end
