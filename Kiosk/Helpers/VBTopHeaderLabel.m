//
//  VBTopHeaderLabel.m
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "VBTopHeaderLabel.h"

@implementation VBTopHeaderLabel
{
    UILabel *stepOfStepsLabel;
    UIView *progressView;
    
    CGFloat weightProgressView;
    CGFloat heightTopHeaderLabel;
}

-(id)init
{
    if (self == [super init]) {
        self = [super init];
        [self defaultSettings];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self == [super initWithCoder:aDecoder]) {
        self = [super initWithCoder:aDecoder];
        [self defaultSettings];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    if (self == [super initWithFrame:frame]) {
        self = [super initWithFrame:frame];
        [self defaultSettings];
    }
    return self;
}

-(void)defaultSettings
{
    _edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    [self setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:24]];
    self.textColor = [UIColor whiteColor];
    self.numberOfLines = 0;
    self.textAlignment = NSTextAlignmentCenter;
    self.backgroundColor = vbBlueColor;
    
    stepOfStepsLabel = [[UILabel alloc] init];
    [stepOfStepsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
    stepOfStepsLabel.textColor = [UIColor whiteColor];
    stepOfStepsLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:stepOfStepsLabel];
    
    progressView = [[UIView alloc] init];
    progressView.backgroundColor = vbOrangeColor;
    [self addSubview:progressView];
}

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, _edgeInsets)];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    size.width  += _edgeInsets.left + _edgeInsets.right;
    size.height += _edgeInsets.top + _edgeInsets.bottom;
    
    [stepOfStepsLabel setFrame:CGRectMake(320 - 50, size.height - 25, 30, 20)];
    [progressView setFrame:CGRectMake(0, size.height - 3, weightProgressView, 3)];
    
    return size;
}

-(void)setHeader:(NSString *)theHeader step:(NSUInteger )theStep fromSteps:(NSUInteger )theFromSteps;
{
    self.text = theHeader;
    stepOfStepsLabel.text = [NSString stringWithFormat:@"%lu / %lu",(unsigned long)theStep, (unsigned long)theFromSteps];
    
    weightProgressView = theStep * self.bounds.size.width / theFromSteps;
    heightTopHeaderLabel = self.frame.size.height + _edgeInsets.top + _edgeInsets.bottom;
    
    NSLog(@"%f",heightTopHeaderLabel);
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.center = CGPointMake(160, heightTopHeaderLabel/2);
                         //self.frame = CGRectMake(0, 0, self.frame.size.width, heightTopHeaderLabel);
                         
                         //[self sizeToFit];
                         //[self sizeThatFits:CGSizeMake(320, heightTopHeaderLabel)];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

-(CGFloat)getHeightLabel
{
    return heightTopHeaderLabel;
}

@end
