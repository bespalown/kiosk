//
//  VBPollStep1.m
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "VBPollStep1VC.h"
#import "VBProgressView.h"

@implementation VBPollStep1VC
{
    NSArray *steps;
    NSUInteger selectedStep;
    VBScrollView *scrollView;
    
    VBProgressView *progressView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if (IS_IPHONE_5) {
        _bottomMarginTextField.constant += 30;
        _bottomOnNext.constant += 0;
    }
    if (IS_IOS8) {
        _bottomOnNext.constant += 40;
    }
    
    _textField.placeholder = NSLocalizedString(@"Например, Bling-blings", nil);
    _textField.borderStyle = UITextBorderStyleNone;
    _textField.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24];
    [_textField becomeFirstResponder];
    _textField.delegate = self;
    _textField.textAlignment = NSTextAlignmentCenter;
    
    [_onNext setTintColor:vbBlueColor];
    [_onNext.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:16]];
    [_onNext setTitle:NSLocalizedString(@"Дальше", nil) forState:UIControlStateNormal];
    
    [self performSelectorInBackground:@selector(initOpros) withObject:nil];
}

-(void)initOpros
{
    selectedStep = 0;
    steps = @[NSLocalizedString(@"Как называется ваш магазин?", nil),
              NSLocalizedString(@"Что вы продаете?", nil),
              NSLocalizedString(@"Придумайте временный логин магазина (потом вы сможете привязять его к своему домену)", nil),
              NSLocalizedString(@"В каком городе вы находитесь?", nil),
              NSLocalizedString(@"Как называется ваш магазин?", nil),
              NSLocalizedString(@"Что вы продаете?", nil),
              NSLocalizedString(@"Придумайте временный логин магазина \n (потом вы сможете привязять его к своему домену)", nil),
              NSLocalizedString(@"Какой у вас средний оборот в месяц? (никто не узнает, обещаем)", nil)
              ];
    
    self.navigationItem.title = [NSString stringWithFormat:@"1 / %lu", (unsigned long)steps.count];
    
    scrollView = [[VBScrollView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 120) andNumberOfPages:steps.count];
    scrollView.VBDelegate = (id)self;
    [self.view addSubview:scrollView];
    scrollView.pagingEnabled = YES;
    [scrollView configureViewAtIndexWithCompletion:^(UIView *view, NSInteger index, BOOL success) {
        view.backgroundColor = [UIColor clearColor];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 20, 280, 80)];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
        label.userInteractionEnabled = NO;
        label.exclusiveTouch = NO;
        label.numberOfLines = 3;
        label.tag = index + 30;
        label.text = steps[index];
        [view addSubview:label];
    }];
    
    progressView = [[VBProgressView alloc] initWithStepsCount:steps.count
                                                        frame:CGRectMake(0,
                                                                         scrollView.frame.size.height,
                                                                         scrollView.frame.size.width,
                                                                         3)];
    [self.view addSubview:progressView];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] bk_initWithTitle:@"Назад" style:UIBarButtonItemStyleBordered handler:^(id sender) {
        if (selectedStep > 0) {
            selectedStep--;
            [scrollView selectIndexPage:selectedStep];
        }
        else
            [self.navigationController popViewControllerAnimated:YES];
    }];
    [leftBarButton setTintColor:[UIColor whiteColor]];
    [self.navigationItem setLeftBarButtonItem:leftBarButton];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] bk_initWithTitle:@"Дальше" style:UIBarButtonItemStyleBordered handler:^(id sender) {
        if (selectedStep < steps.count - 1) {
            selectedStep++;
            [scrollView selectIndexPage:selectedStep];
        }
    }];
    [rightBarButton setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem:rightBarButton];
}

-(void)VBScrollView:(VBScrollView *)theScrollView didSelectIndexPage:(NSInteger)theIndexPage
{
    self.navigationItem.title = [NSString stringWithFormat:@"%lu / %lu",(unsigned long)theIndexPage + 1, (unsigned long)steps.count];
    [progressView setStep:theIndexPage animated:YES];
    NSLog(@"%d",theIndexPage);
}

-(void)viewDidDisappear:(BOOL)animated
{
    [_textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    //[self performSegueWithIdentifier:@"segDetail" sender:self];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self onNext:nil];
    return YES;
}

@end
