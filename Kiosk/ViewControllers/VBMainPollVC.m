//
//  ViewController.m
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "VBMainPollVC.h"

@interface VBMainPollVC ()

@end

@implementation VBMainPollVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if (!IS_IPHONE_5) {
        _topMargin.constant = _bottomMargin.constant = 0;
    }
    
    self.navigationItem.title = @"Киоск";
    
    _headerLabel.text = NSLocalizedString(@"Несколько \n вопросов перед тем, \n как начать", nil);
    _headerLabel.textColor = [UIColor whiteColor];
    _headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24];
    
    [_onStart setTitle:NSLocalizedString(@"Начать", nil) forState:UIControlStateNormal];
    [_onStart setTintColor:[UIColor whiteColor]];
    [_onStart.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:16]];
    
    [self.view setBackgroundColor:vbBlueColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onStart:(id)sender {
    [self performSegueWithIdentifier:@"segDetail" sender:self];
}
@end
