//
//  VBPoolStep3VC.m
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "VBPoolStep3VC.h"

@implementation VBPoolStep3VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [_topHeaderLabel setHeader:NSLocalizedString(@"Придумайте временный логин магазина \n (потом вы сможете привязять его к своему домену)", nil) step:3 fromSteps:8];
    
    _textField.placeholder = NSLocalizedString(@"Например, myshop", nil);
    _textField.borderStyle = UITextBorderStyleNone;
    _textField.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24];
    [_textField becomeFirstResponder];
    _textField.delegate = self;
    
    [_onNext setTintColor:vbBlueColor];
    [_onNext.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:16]];
    [_onNext setTitle:NSLocalizedString(@"Дальше", nil) forState:UIControlStateNormal];
    
    if  (IS_IPHONE_5) {
        _marginTextField.constant += 0;
    }
    else {
        _marginTextField.constant -= 25;
    }
    if (IS_IOS8) {
        _marginTextField.constant -= 10;
        _marginBottomOnNext.constant += 40;
        if (!IS_IPHONE_5) {
            _marginBottomOnNext.constant -= 40;
        }
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [_textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    [self performSegueWithIdentifier:@"segDetail" sender:self];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self onNext:nil];
    return YES;
}

@end
