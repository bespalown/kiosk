//
//  VBPoolStep3VC.h
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBMasterPollVC.h"

@interface VBPoolStep3VC : VBMasterPollVC
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginBottomOnNext;

@property (weak, nonatomic) IBOutlet VBTopHeaderLabel *topHeaderLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *onNext;

- (IBAction)onNext:(id)sender;

@end
