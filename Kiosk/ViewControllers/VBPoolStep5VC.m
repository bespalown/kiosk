//
//  VBPoolStep5VC.m
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "VBPoolStep5VC.h"

@implementation VBPoolStep5VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [_topHeaderLabel setHeader:NSLocalizedString(@"Какой у вас средний оборот в месяц? (никто не узнает, обещаем)", nil) step:5 fromSteps:8];
    
    [_onNext setTintColor:vbBlueColor];
    [_onNext.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:16]];
    [_onNext setTitle:NSLocalizedString(@"Дальше", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"В разработке" message:@"все начинается сначала" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self performSegueWithIdentifier:@"segDetail" sender:self];
}

@end
