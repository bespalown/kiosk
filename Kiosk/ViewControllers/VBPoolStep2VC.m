//
//  VBPoolStep2VC.m
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "VBPoolStep2VC.h"

@implementation VBPoolStep2VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [_topHeaderLabel setHeader:NSLocalizedString(@"Что вы продаете?", nil) step:2 fromSteps:8];
    
    [_onNext setTintColor:vbBlueColor];
    [_onNext.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:16]];
    [_onNext setTitle:NSLocalizedString(@"Дальше", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    [self performSegueWithIdentifier:@"segDetail" sender:self];
}

@end
