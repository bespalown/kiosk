//
//  VBMasterPollVC.h
//  Kiosk
//
//  Created by bespalown on 06/10/14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBTopHeaderLabel.h"

@interface VBMasterPollVC : UIViewController <UITextFieldDelegate>

-(void)navigationBarButtons;

@end
